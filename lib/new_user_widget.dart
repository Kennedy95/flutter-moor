import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:provider/provider.dart';

import 'data/moor_database.dart';

class NewUserInput extends StatefulWidget {
  const NewUserInput({
    Key key,
  }) : super(key: key);

  @override
  _NewUserInputState createState() => _NewUserInputState();
}

class _NewUserInputState extends State<NewUserInput> {
  String _name = "";
  TextEditingController nameController;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[nameInput(), submitButton(context)],
    );
  }

  Widget nameInput() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              border: Border.all(
                color: Colors.blue,
              ),
              borderRadius: BorderRadius.circular(10.0)),
          height: 50.0,
          child: TextField(
            controller: nameController,
            keyboardType: TextInputType.emailAddress,
            textAlign: TextAlign.start,
            style: TextStyle(
              color: Colors.black,
              fontSize: 17,
            ),
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(left: 10.0, right: 10.0),
              hintText: 'Name',
              hintStyle: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget submitButton(BuildContext context) {
    return Container(
      width: 80.0,
      height: 40.0,
      margin: EdgeInsets.only(right: 10.0),
      child: RaisedButton(
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(5.0)),
        onPressed: () async {
          if (nameController.text.toString().isEmpty) {
            Fluttertoast.showToast(
                msg: 'Enter name',
                toastLength: Toast.LENGTH_SHORT,
                timeInSecForIosWeb: 1,
                gravity: ToastGravity.BOTTOM,
                fontSize: 16.0);
          } else {
            _name = nameController.text.toString();
            final dao = Provider.of<UsersDao>(context,listen: false);
            final user = UsersCompanion(name: Value(_name));
            dao.insertUser(user);
            Fluttertoast.showToast(
                msg: 'User saved successfully',
                toastLength: Toast.LENGTH_SHORT,
                timeInSecForIosWeb: 1,
                gravity: ToastGravity.BOTTOM,
                fontSize: 16.0);
            setState(() {
              nameController.clear();
            });
          }
        },
        padding: EdgeInsets.all(0),
        textColor: Colors.white,
        child: Ink(
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(5.0), color: Colors.blue),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              'Submit',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
