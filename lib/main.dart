import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:moor_sample/new_user_widget.dart';
import 'package:provider/provider.dart';

import 'data/moor_database.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final db = AppDatabase();

    return MultiProvider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Moor Example',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'Moor Example'),
      ),
      providers: [
        Provider(
          // The single instance of AppDatabase
          create: (_) => db.usersDao,
        ),
      ],
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          NewUserInput(),
          Expanded(child: _buildUserList(context)),
        ],
      ),
    );
  }

  StreamBuilder<List<User>> _buildUserList(BuildContext context) {
    final dao = Provider.of<UsersDao>(context);
    return StreamBuilder(
      stream: dao.getAllUser(),
      builder: (context, AsyncSnapshot<List<User>> snapshot) {
        final tasks = snapshot.data ?? List();

        return ListView.builder(
          itemCount: tasks.length,
          itemBuilder: (_, index) {
            final item = tasks[index];
            return _buildListItem(item, dao);
          },
        );
      },
    );
  }

  Widget _buildListItem(User item, UsersDao dao) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () {
            Fluttertoast.showToast(
                msg: 'User deleted succeessfully',
                toastLength: Toast.LENGTH_SHORT,
                timeInSecForIosWeb: 1,
                gravity: ToastGravity.BOTTOM,
                fontSize: 16.0);
            return dao.deleteUser(item);
          },
        )
      ],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              item.name,
              style: TextStyle(
                fontSize: 17,
              ),
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}
